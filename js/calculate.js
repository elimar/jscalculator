jQuery(document).ready(function() {

    function setPPA(x)  {
        m=x
    }
    setPPA(12);
    function CalcRate(credit,repay,numberOfPayments) {
        debugger
        var p= credit;
        var i= 0 ;
        var a= repay;
        var n= numberOfPayments;
        var f= 0;

        //Isaac's magic ...
        var x=1.0001; var fx=0; var dx=0; var z=0;
        do {
            fx=i+a*(Math.pow(x,n+1)-x)/(x-1)+f*Math.pow(x,n)-p;
            dx=a*(n*Math.pow(x,n+1)-(n+1)*Math.pow(x,n)+1)/Math.pow(x-1,2)+n*f*Math.pow(x,n-1);
            z=fx/dx; x=x-z;
            //alert("fx="+fx+"\ndx="+dx+"\nz="+z+"\nx="+x)
        }
        while (Math.abs(z)>1e-9);
        r=100*(Math.pow(1/x,m)-1);

        return TwoDP(r);
    }

    function TwoDP(num) {
        if (isNaN(num)) num="0"
        num="$"+Math.round(100*num)/100;
        if (num.indexOf(".")==-1) num+=".00";
        if (num.indexOf(".")==num.length-2) num+="0";
        return num.substring(1,num.length);
    }

    function OneDP(num) {
        if (isNaN(num)) num="0"
        num="$"+Math.round(10*num)/10;
        if (num.indexOf(".")==-1) num+=".0";
        return num.substring(1,num.length);
    }





    var
        // слайдер суммы
        summ = jQuery('#slider-summ'),
        summMin = 1000,
        summMax = 10000,
        summStep = 500,
        summValue = 1000,

        // сумма займа
        summSet = jQuery("#summ-set"),

        // слайдер срока
        date = jQuery('#slider-date'),
        dateMin = 1,
        dateMax = 3,
        dateStep = 1,
        dateValue = 1,

        // срок займа
        dateSet = jQuery("#date-set"),
        // плата за заем
        diffValue,
        // сумма возврата
        refSumm,
        // месячная сумма возврата
        refSummMonth,
        // год. процентная ставка
        rpsn,

        // сумма возврата вывод
        returnSumm = jQuery('#return-summ'),
        // сумма переплаты вывод
        returnDiff = jQuery('#return-diff'),
        returnRpsn = jQuery('#rpsn'),

        // отступ у слайдера
        margin  = (summ.outerWidth(true) - summ.width()) / 2,
        resizeTimeoutId;

    function sepNumber(str){
        str = parseInt(str).toString();
        return str.replace(/(\d)(?=(\d\d\d)+([^\d]|jQuery))/g, 'jQuery1 ');
    }

    function result(){
        summSet.val(summValue);
        dateSet.val(dateValue);

        if (dateValue == 1){
            refSummMonth = (summValue + (summValue * 0.365/12) + (summValue * 0.18)) ;
        }
        if (dateValue >= 2 && dateValue <= 6){
            refSummMonth = ((summValue * 0.365/12)/(1-Math.pow(1+0.365/12 ,(-dateValue))) + summValue * 0.18);
        }
        if(dateValue >= 7 && dateValue <= 12){
            refSummMonth = ((summValue * 0.365/12)/(1-Math.pow(1+0.365/12 ,(-dateValue))) + summValue * 0.18);
        }
        refSumm = refSummMonth*dateValue;
        rpsn = CalcRate(summValue,Math.floor(refSummMonth),dateValue);
        //rpsn = rpsn[dateValue][summValue];

        // вывели расчеты
        returnRpsn.text(rpsn);
        returnDiff.text(sepNumber(refSummMonth));
        returnSumm.text(sepNumber(refSumm));
    }

    jQuery.ui.slider.prototype.widgetEventPrefix = 'slider';

    function setSlider(){
        // нижнии крайние значения
        jQuery('.calkulator__summ-min').text(sepNumber(summMin));
        jQuery('.calkulator__summ-max').text(sepNumber(summMax));
        jQuery('.calkulator__date-min').text(dateMin);
        jQuery('.calkulator__date-max').text(dateMax);

        // инициализация слайдеров
        summ.slider({
            range: 'min',
            min: summMin,
            max: summMax,
            step: summStep,
            value: summValue,
            slide: function(event,ui) {
                summValue = ui.value;
                result();
            }
        });

        jQuery('#slider-date').slider({
            range: 'min',
            min: dateMin,
            max: dateMax,
            step: dateStep,
            value: dateValue,
            slide: function(event,ui) {
                dateValue = ui.value;
                result();
            }
        });
        result();
    }

    jQuery('.calkulator__input-box').children('.input').on('keyup blur',function(){
        var t = jQuery(this);
        setTimeout(function(){
            t.parent().toggleClass('calkulator__input-box--placeholder',Boolean(t.val()));
        });
    }).trigger('blur');

    // перестроить слайдер
    jQuery(window).on('resize', function(){
        clearTimeout(resizeTimeoutId);
        resizeTimeoutId = setTimeout(function(){
            margin  = (summ.outerWidth(true) - summ.width()) / 2;
            setSlider(false);
        }, 1000);
    });


    // ввод суммы и даты в инпуте
    jQuery("#summ-set , #date-set").on('change keydown blur', function(event) {
        if (event.keyCode == 13) {
            jQuery(this).trigger('blur');
        }
        if (event.type == 'blur') {
            var v = this.value;
            if (v.match(/[^0-9]/g))
                v = v.replace(/[^0-9]/g, '');
            if(jQuery(this).is('#summ-set')){
                if(v>summMax)
                    v = summMax;
                if(v<summMin)
                    v = summMin;
                summValue = v;
                summ.slider('value',v);
            }
            else {
                if(v>dateMax)
                    v = dateMax;
                if(v<dateMin)
                    v = dateMin;
                var arithmetic = (v - dateMin) % dateStep;
                if (arithmetic > 0){
                    v = parseInt((v - dateMin) / dateStep) * dateStep + dateMin;
                    if (arithmetic * 2 > dateStep){
                        v += dateStep;
                    }
                }
                dateValue = v;
                date.slider('value',v);
            }
            result();
        }
    });

    setSlider(true);

});




